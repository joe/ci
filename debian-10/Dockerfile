FROM debian:buster
MAINTAINER Ansgar.Burchardt@tu-dresden.de
RUN rm -f /etc/apt/apt.conf.d/docker-gzip-indexes \
  && rm -rf /var/lib/apt/lists/*
RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  autoconf \
  automake \
  bison \
  build-essential \
  gcc-7 \
  g++-7 \
  gfortran-7 \
  ca-certificates \
  clang-6.0 \
  clang-7 \
  libc++-7-dev \
  libc++abi-7-dev \
  cmake \
  coinor-libipopt-dev \
  curl \
  flex \
  gfortran \
  git \
  git-lfs \
  gnuplot-nox \
  libadolc-dev \
  libalberta-dev \
  libarpack++2-dev \
  libboost-dev \
  libboost-program-options-dev \
  libboost-serialization-dev \
  libboost-system-dev \
  libffi-dev \
  libgtest-dev \
  libisl-dev \
  libltdl-dev \
  libopenblas-dev \
  libscotchmetis-dev \
  libscotchparmetis-dev \
  libsuitesparse-dev \
  libsuperlu-dev \
  libtinyxml2-dev \
  libtool \
  locales-all \
  mpi-default-bin \
  mpi-default-dev \
  ninja-build \
  openssh-client \
  pkg-config \
  python-dev \
  python-numpy \
  python-pip \
  python-setuptools \
  python-virtualenv \
  python-vtk6 \
  python-wheel \
  python3 \
  python3-dev \
  python3-matplotlib \
  python3-mpi4py \
  python3-numpy \
  python3-pip \
  python3-pytest \
  python3-setuptools \
  python3-scipy \
  python3-venv \
  python3-wheel \
  vc-dev \
  libgmp-dev \
  libeigen3-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN adduser --disabled-password --home /duneci --uid 50000 duneci

# The environment for Dune builds is set up in base-common/dune-setup.dockerfile,
# which gets appended to this file during the build process
